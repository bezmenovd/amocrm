<?php

namespace Msnet\Amocrm;

use Msnet\Amocrm\Base\Entity;

/**
 * https://www.amocrm.ru/developers/content/api/contacts
 */
class Contact extends Entity
{
    protected $data =
    [
        'name' => '',
        'responsible_id' => '',
        'created_by' => 0,
        'created_at' => 0,
        'tags' => [],
        'leads_id' => [],
        'company_id' => 0,
        'custom_fields' => [],
    ];

    use Setters\Name;
    use Setters\CreatedAt;
    use Setters\UpdatedAt;
    use Setters\Tags;
    use Setters\CustomFields;
}