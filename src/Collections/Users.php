<?php

namespace Msnet\Amocrm\Collections;

use Msnet\Amocrm\Base\Collection;

class Users extends Collection
{
    /**
     * @param string $name
     */
    public function getByName(string $name)
    {
        return $this->find(['name' => $name]);
    }
}