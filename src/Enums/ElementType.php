<?php

namespace Msnet\Amocrm\Enums;

class ElementType
{
    const CONTACT = 1;
    const LEAD = 2;
    const COMPANY = 3;
    const TASK = 4;
    const CUSTOMER = 12;
}