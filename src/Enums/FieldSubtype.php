<?php

namespace Msnet\Amocrm\Enums;

class FieldSubtype
{
    const COUNTRY = 'country';
    const STATE = 'state';
    const CITY = 'city';
    const ZIP = 'zip';
    const ADDRESS_LINE_1 = 'address_line_1';
    const ADDRESS_LINE_2 = 'address_line_2';
}