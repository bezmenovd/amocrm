<?php

namespace Msnet\Amocrm\Enums;

class FieldPhoneType
{
    const WORK = 'WORK';
    const WORKDD = 'WORKDD';
    const MOB = 'MOB';
    const FAX = 'FAX';
    const HOME = 'HOME';
    const OTHER = 'OTHER';
}