<?php

namespace Msnet\Amocrm;

use Msnet\Amocrm\Base\Entity;
use Msnet\Amocrm\Setters;

/**
 * https://www.amocrm.ru/developers/content/api/notes
 */
class Note extends Entity
{
    protected $data = 
    [
        'id' => 0,
        'note_type' => 0,
        'element_id' => 0,
        'element_type' => 0,
        'text' => '',
        'attachment' => '',
    ];
    
    use Setters\Id;
    use Setters\ElementId;
    use Setters\ElementType;
    
    /**
     * @param int $note_type Тип события
     */
    public function setType(int $note_type)
    {
        $this->data['note_type'] = $note_type;
    }

    /**
     * @param string $text Текст
     */
    public function setText(string $text)
    {
        $this->data['text'] = $text;
    }

    /**
     * @param string $attachment Путь к файлу
     */
    public function setAttachment(string $attachment)
    {
        $this->data['attachment'] = $attachment;
    }
}