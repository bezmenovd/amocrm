<?php

namespace Msnet\Amocrm\Managers;

use Msnet\Amocrm\Base\Manager;
use Msnet\Amocrm\Base\Request;
use Msnet\Amocrm\File;
use Msnet\Amocrm\Collections;

class Files extends Manager
{
    /**
     * @param File $file Объект типа File
     */
    public function add(File $file)
    {
        $data = $file->getData();

        /**
         * Первый запрос (загрузка файла)
         */
        $params = [
            'ACTION' => 'ADD_NOTE',
            'ELEMENT_ID' => $data->element_id,
            'ELEMENT_TYPE' => $data->element_type,
            'BODY' => $data->name
        ];

        $url = "private/notes/upload.php?" . http_build_query($params) . "&fileapi" . str_replace('.', '', microtime(true));

        $temp = sys_get_temp_dir() . DIRECTORY_SEPARATOR . time() . $data->name;
        file_put_contents($temp, file_get_contents($data->path));

        $request = $this->client->createBaseRequest("POST", $url);

        $tempResult = $request->execute([
            'UserFile' => new \CURLFile($temp, null, $data->name),
            '_UserFile' => $data->name
        ]);

        if (
            !$tempResult->isSuccess() 
            || 
            $tempResult->getData()['status'] !== 'ok' 
            || 
            empty($tempResult->getData()['note']['params']['link']))
        {
            return $tempResult;
        }

        /**
         * Второй запрос (прикрепление файла)
         */
        $params = array(
            'ACTION' => 'ADD_NOTE',
            'DATE_CREATE' => time(),
            'ATTACH' => $tempResult->getData()['note']['params']['link'],
            'BODY' => $tempResult->getData()['note']['params']['text'],
            'ELEMENT_ID' => $data->element_id,
            'ELEMENT_TYPE' => $data->element_type,
        );

        $request = $this->client->createBaseRequest("POST", 'private/notes/edit2.php');

        return $request->execute($params);
    }
}