<?php

namespace Msnet\Amocrm\Managers;

use Msnet\Amocrm\Base\Manager;
use Msnet\Amocrm\Note;
use Msnet\Amocrm\Collections;

class Notes extends Manager
{
    /**
     * @param array $notes Массив объектов типа Note
     */
    public function add(array $notes)
    {
        $data = [
            'add' => $this->getObjectsData($notes, Note::class)
        ];

        return $this->sendRequest('POST', 'notes', $data);
    }

    /**
     * @param array $notes Массив объектов типа Note (у каждой примечания должен быть задан id)
     */
    public function update(array $notes)
    {
        $data = [
            'update' => $this->getObjectsData($notes, Note::class)
        ];

        return $this->sendRequest('POST', 'notes', $data);
    }

    /**
     * @param int $id ID примечания
     */
    public function get(int $id = null)
    {
        $response = $this->sendRequest(
            'POST', 'notes', $id ? ['id' => $id]: null
        );
        
        if ($response->isSuccess())
        {
            $result = [];

            foreach ($response->getData()['_embedded']['items'] as $id => $item)
                $result[] = new Note($item, $id);
        }

        return $result ? new Collections\Notes($result) : false;
    }
}