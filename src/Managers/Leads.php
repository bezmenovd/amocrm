<?php

namespace Msnet\Amocrm\Managers;

use Msnet\Amocrm\Base\Manager;
use Msnet\Amocrm\Lead;
use Msnet\Amocrm\Collections;

class Leads extends Manager
{
    /**
     * @param array $leads Массив объектов типа Lead
     */
    public function add(array $leads)
    {
        $data = [
            'add' => $this->getObjectsData($leads, Lead::class)
        ];

        return $this->sendRequest('POST', 'leads', $data);
    }

    /**
     * @param array $leads Массив объектов типа Lead (у каждой сделки должен быть задан id)
     */
    public function update(array $leads)
    {
        $data = [
            'update' => $this->getObjectsData($leads, Lead::class)
        ];

        return $this->sendRequest('POST', 'leads', $data);
    }

    /**
     * @param int $id ID сделки
     */
    public function get(int $id = null)
    {
        $response = $this->sendRequest(
            'POST', 'leads', $id ? ['id' => $id]: null
        );
        
        if ($response->isSuccess())
        {
            $result = [];

            foreach ($response->getData()['_embedded']['items'] as $id => $item)
                $result[] = new Lead($item, $id);
        }

        return $result ? new Collections\Leads($result) : false;
    }
}