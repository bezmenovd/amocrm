<?php

namespace Msnet\Amocrm\Managers;

use Msnet\Amocrm\Base\Manager;
use Msnet\Amocrm\Pipeline;
use Msnet\Amocrm\Collections;

class Pipelines extends Manager
{
    /**
     * @param array $pipelines Массив объектов типа Pipeline
     */
    public function add(array $pipelines)
    {
        $data = [
            'add' => $this->getObjectsData($pipelines, Pipeline::class)
        ];

        return $this->sendRequest('POST', 'pipelines', $data);
    }

    /**
     * @param array $pipelines Массив объектов типа Pipeline (у каждой воронки должен быть задан id)
     */
    public function update(array $pipelines)
    {
        $data = [
            'update' => $this->getObjectsData($pipelines, Pipeline::class)
        ];

        return $this->sendRequest('POST', 'pipelines', $data);
    }

    /**
     * @param int $id ID воронки
     */
    public function get(int $id = null)
    {
        $response = $this->sendRequest(
            'GET', 'pipelines', $id ? ['id' => $id]: null
        );
        
        if ($response->isSuccess())
        {
            $result = [];
            
            foreach ($response->getData()['_embedded']['items'] as $id => $item)
                $result[] = new Pipeline($item, $id);
        }

        return $result ? new Collections\Pipelines($result) : false;
    }
}