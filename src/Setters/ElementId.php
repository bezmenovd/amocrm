<?php

namespace Msnet\Amocrm\Setters;

trait ElementId
{
    /**
     * @param int $element_id Идентификатор элемента
     */
    public function setElementId(int $element_id)
    {
        $this->data['element_id'] = $element_id;
    }
}