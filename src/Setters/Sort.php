<?php

namespace Msnet\Amocrm\Setters;

trait Sort
{    
    /**
     * @param int $sort Порядковый номер
     */
    public function setSort(int $sort)
    {
        $this->data['sort'] = $sort;   

        return $this;
    }
}