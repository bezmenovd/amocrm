<?php

namespace Msnet\Amocrm\Setters;

trait Tags
{
    /**
     * @param array $tags Массив тегов (строки)
     */
    public function setTags(array $tags)
    {
        $this->data['tags'] = join(", ", $tags);   

        return $this;
    }

    /**
     * @param string $tag Тег
     */
    public function addTag(string $tag)
    {
        $this->data['tags'] .= $this->data['tags'] ? ", $tag" : $tag;   

        return $this;
    }
}