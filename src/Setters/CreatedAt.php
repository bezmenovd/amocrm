<?php

namespace Msnet\Amocrm\Setters;

use Msnet\Amocrm\Field;

trait CreatedAt
{
    /**
     * @param string $date Unix время создания
     */
    public function setCreatedAt(string $date)
    {
        if (strlen($date) !== 10)
            throw new \Exception("Invalid date: $date");

        $this->data['created_at'] = $date;   

        return $this;
    }
}