<?php

namespace Msnet\Amocrm\Setters;

trait Id
{
    /**
     * @param int $id Идентификатор
     */
    public function setId(int $id)
    {
        $this->data['id'] = $id;   

        return $this;
    }
}