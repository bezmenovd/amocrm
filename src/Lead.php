<?php

namespace Msnet\Amocrm;

use Msnet\Amocrm\Base\Entity;
use Msnet\Amocrm\Base\Client;
use Msnet\Amocrm\Pipeline;
use Msnet\Amocrm\Setters;
use Msnet\Amocrm\Elements\CustomField;

/**
 * https://www.amocrm.ru/developers/content/api/leads
 */
class Lead extends Entity
{
    protected $data = 
    [
        'id' => 0,
        'name' => '',
        'created_at' => '',
        'updated_at' => '',
        'status_id' => 0,
        'pipeline_id' => 0,
        'responsible_user_id' => 0,
        'sale' => 0,
        'tags' => '',
        'contacts_id' => [],
        'company_id' => '',
        'catalog_elements_id' => [],
        'custom_fields' => []
    ];

    use Setters\Id;
    use Setters\Name;
    use Setters\CreatedAt;
    use Setters\UpdatedAt;
    use Setters\Tags;
    use Setters\CustomFields;

    /**
     * @param int $pipeline_id Идентификатор воронки продаж (Msnet\Amocrm\PipelineStatus get())
     */
    public function setPipelineId(int $pipeline_id)
    {
        $this->data['pipeline_id'] = $pipeline_id;
    }
    /**
     * @param int $status_id Идентификатор статуса воронки продаж (Msnet\Amocrm\PipelineStatus get())
     */
    public function setStatusId(int $status_id)
    {
        $this->data['status_id'] = $status_id;
    }

    /**
     * @param int $responsible_user_id Идентификатор пользователя, ответственного за сделку 
     */
    public function setResponsibleUserId(int $responsible_user_id)
    {
        $this->data['responsible_user_id'] = $responsible_user_id;
    }

    /**
     * @param int $sale Бюджет сделки
     */
    public function setSale(int $sale)
    {
        $this->data['sale'] = $sale;
    }

    /**
     * @param int $contact_id ID контакта сделки
     */
    public function addContact(int $contact_id)
    {
        $this->data['contacts_id'][] = $contact_id;
    }
}