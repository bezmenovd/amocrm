<?php

namespace Msnet\Amocrm\Base;

class Entity
{
    /**
     * @var array $data
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data = [], int $id = null)
    {
        foreach ($data as $key => $value)
        {
            if (key_exists($key, $this->data))
                $this->data[$key] = $value;
        }

        if (!is_null($id) && key_exists('id', $this->data))
            $this->data['id'] = $id;
    }

    /**
     * @return object
     */
    public function getData()
    {
        return (object)$this->data;
    }

    /**
     * @param string $name
     */
    public function getField(string $name)
    {
        return $this->data[$name];
    }

    /**
     * @param string $name
     */
    public function removeField(string $name)
    {
        unset($this->data[$name]);
    }

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function setField(string $name, $value)
    {
        $this->data[$name] = $value;
    }
    
}