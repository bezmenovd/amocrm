<?php

namespace Msnet\Amocrm\Base;

use Msnet\Amocrm;
use Msnet\Amocrm\Base\Request;
use Msnet\Amocrm\Base\Timer;

class Client
{
    /**
     * @var string $account
     */
    protected $account;

    /**
     * @var array $accountData
     */
    protected $accountData;

    /**
     * @var bool authorized
     */
    protected $authorized;

    /**
     * @var string $baseUrl
     */
    protected $baseUrl;

    /**
     * @var string $baseApiUrl
     */
    protected $baseApiUrl;

    /**
     * @var Timer $timer
     */
    protected $timer;

    /**
     * @param string $account Имя пользователя (поддомен)
     * @param string $email   Адрес электронной почты
     * @param string $hash    Ключ api
     */
    public function __construct(string $account, string $email, string $hash)
    {
        $this->account = $account;
        $this->baseUrl = "https://$account.amocrm.ru/";
        $this->baseApiUrl = "{$this->baseUrl}api/v2/";

        if (!$this->authorize($email, $hash))
            throw new \Exception("Amocrm client not authorized");

        /**
         * Ограничение количества запросов на 5 в секунду
         */
        $this->timer = new Timer(1000, 5);
    }

    /**
     * @param string $email   Адрес электронной почты
     * @param string $hash    Ключ api
     */
    public function authorize(string $email, string $hash)
    {
        $response = (
            new Request("POST", "{$this->baseUrl}private/api/auth.php?type=json")
        )->execute([
            'USER_LOGIN' => $email,
            'USER_HASH' => $hash,
        ]);

        if ($this->authorized = $response->isSuccess())
            $this->accountData = (object)$response->getData()['response']['accounts'][0];

        return $this->authorized;
    }

    /**
     * @return bool
     */
    public function isAuthorized()
    {
        return $this->authorized;
    }

    /**
     * @return array
     */
    public function getAccountData()
    {
        return $this->accountData;
    }

    /**
     * @param string $method
     * @param string $action
     * 
     * @return Request
     */
    public function createRequest(string $method, string $action)
    {
        $this->timer->tick();
        return new Request($method, $this->baseApiUrl . $action);
    }

    /**
     * @param string $method
     * @param string $action
     * 
     * @return Request
     */
    public function createBaseRequest(string $method, string $action)
    {
        $this->timer->tick();
        return new Request($method, $this->baseUrl . $action);
    }

    /**
     * @return string
     */
    public function getBaseApiUrl()
    {
        return $this->baseApiUrl;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }
}